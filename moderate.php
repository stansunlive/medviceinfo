<?php
require 'config.php';
session_start();
$action = isset($_GET['action']) ? $_GET['action'] : "";
$username = isset($_SESSION['username']) ? $_SESSION['username'] : "";
$auth = isset($_SESSION['isauth']) ? $_SESSION['isauth'] : false;

switch ($action) {
    case 'logout':
        logout();
        break;
    case 'moderate':
        moderate($auth);
        break;
    case 'succ':
        succ($_GET['id']);
        break;
    case 'deny':
        deny($_GET['id']);
        break;
    default :
        login($auth);
        break;
}

function logout() {
    $_SESSION['isauth'] = false;
    header("Location:moderate.php");
}

function moderate($auth) {
    if(!$auth) {
        login($auth);
        exit;
    }
    $elements = array();
    $elements['page'] = (isset($_GET["page"])) ? (int)$_GET["page"] : 1;
    $elements['articles'] = Article::getListToModerate();
    $elements['title'] = "Список всех статей";
    require (TEMPLATE_PATH . 'mall.php');
}


function login($auth) {
    $elements = array();
    $elements['title'] = "Авторизация для модератора";
    if($auth){
        moderate($auth);
        exit;
    } else {
        $elements['title'] = "Вход для модератора";
        if(isset($_POST['login'])){
            if( ($_POST['login'] === MODERATOR_USERNAME) && ($_POST['password'] === MODERATOR_PASSWORD) ) {
                $_SESSION['isauth'] = true;
                header("Location:moderate.php");
            }
        } else {
            require (TEMPLATE_PATH . 'mlogin.php');
        }
    }
}

function succ($id) {
    Article::setStatus($id, 1);
    header("Location:moderate.php");
}

function deny($id) {
    Article::setStatus($id, -1);
    header("Location:moderate.php");
}

?>