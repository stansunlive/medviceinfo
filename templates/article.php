<?php include "includes/header.php"; ?>
<?php $article = $elements['article']; ?>
<div title='Вернуться к разделу "Это интересно"' id="bl-hgoback" onclick="location.href='?action=none'" class="st-mi st-scroll-fix"></div>
<div id="bl-article">
    <div class="bl-author st-t st-fs">
        <div class="bl-avatar">
            <img src="<?=$article->avatar?>" alt="Аватар"/>
        </div>
        <div class="bl-name"><?=$article->author?></div>
        <div class="bl-specialty st-tdark"><?=$article->specialization?></div>
        <div class="bl-material st-tblack">
            <h1 class="bl-title"><?=$article->title?></h1>
            <p><?=$article->content?></p>
            <img src="<?=$article->image?>" alt="article-img" height="400" class="article-img"/>
        </div>
    </div>
</div>
<?php include "includes/footer.php"; ?>