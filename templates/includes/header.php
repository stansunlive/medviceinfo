<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<link rel="stylesheet" href="templates/includes/styles/style.css">
		<link rel="stylesheet" href="templates/includes/styles/style2.css">
		<link rel="stylesheet" href="templates/includes/styles/style3.css">
		<script src="templates/includes/jquery1.10.2.min.js"></script>
		<?php if($_GET['action'] == 'create') { ?>
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <?php } ?>
		<link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	    <title><?= htmlspecialchars($elements['title'])?></title>
	</head>
	<body>
		<div id="up-mainarea">
		<div id="up-content">
			<table id="st-prospect" class="st-etable">
			<tbody><tr>
				<td class="st-bg rl-left">
					<header>
						<div id="rl-left" class="st-bg st-fixed" style="left: 0px;">
						<table id="st-prospect" class="st-etable rl-left">
							<tbody><tr>
								<td class="rl-top">
									<a id="logo" href="http://medvice.info/" class="mv-logo">
										<div class="st-mi"></div>
									</a>
								</td>
							</tr>
							<tr>
								<td class="rl-center">
								<div id="rl-menu">
									<ul>
										<li><a href="http://medvice.info/" id="rl-link-1" class="st-fs"><div class="st-mi"></div><div>Главная</div></a></li>
										<li><a href="http://medvice.info/select" id="rl-link-7" class="st-fs "><div class="st-mi"></div><div>Наши Врачи</div></a></li>
										<li><a href="http://localhost/medvice" id="rl-link-4" class="st-fs mv-active"><div class="st-mi"></div><div>Это интересно</div></a></li>
										<li><a href="http://medvice.info/agreement" id="rl-link-2" class="st-fs"><div class="st-mi"></div><div>Соглашение</div></a></li>
										<li><a href="http://medvice.info/project" id="rl-link-3" class="st-fs "><div class="st-mi"></div><div>О проекте</div></a></li>
									</ul>
								</div>
								</td>
							</tr>
							<tr>
								<td class="rl-bottom">
									<div class="rl-copyright st-twhite">© 2014 - 2015 ООО "Медвайс"</div>
								</td>
							</tr>
							</tbody>
						</table>
						</div>
					</header>		
				</td>
				<td class="rl-right">
					<article>
						<div id="hd-toolbar" class="st-t st-fs st-bgwhite st-fixed" style="left: 220px;">
						<h1>Это интересно</h1>					
						<div id="hd-profile">
						<?php if( isset($_GET['action']) && ($_GET['action']==="personal") && ($_SESSION['auth'] === true) ) { ?>
							<table class="st-etable">
							<tbody><tr><td>
								<a href="?action=logout" title="Выйти из системы" class="btn effect orange">Выход</a>
								</td>
							</tr></tbody></table>
						<?php } else { ?>
							<table class="st-etable">
								<tbody><tr><td>
                    				<a href="http://medvice.info/" title="Задать вопрос врачу" class="btn effect orange">Задать вопрос врачу</a>
								</td>
								<td>
									<a href="?action=personal" title="Перейти на страницу авторизации" class="hd-unauthor st-t st-fs">Личный кабинет</a>
								</td>
								<td>
									<a href="http://medvice.info/registration" title="Перейти на страницу регистрации" class="hd-unauthor  st-t st-fs">Регистрация</a>
								</td>
							</tr></tbody></table> 
						<?php } ?>
						</div> 
						</div>
					<div id="rl-content" class="st-fs st-t st-bgwhite">