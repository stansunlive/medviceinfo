<?php include "includes/header.php"; ?>

<form class="logf" action="moderate.php" method="post">
    <input type="hidden" name="login" value="true"/>
    <?php if(isset($results['error'])) { ?>
    <div class="erm"><?= $elements['error'] ?></div>
    <?php } ?>
    <table class="ftbl">
        <tr>
        <th>Авторизация для <br> модератора</th>
        </tr>
        <tr>
            <td>
                <input class="st-input-light st-t st-fs st-w250"
                       type="text"
                       name="login"
                       id="login"
                       placeholder="Введите логин"
                       required autofocus maxlength="20"/>
            </td>
        </tr>
        <tr>
            <td>
                <input class="st-input-light st-t st-fs st-w250"
                       type="password"
                       name="password"
                       id="password"
                       placeholder="Введите пароль"
                       required maxlength="20"/>
            </td>           
        </tr>
        <tr>
            <td align="center">
                <input class="ftbb" type="submit" name="loginbutton" value="Войти..."/>
            </td>
        </tr>
</form>

<?php include "includes/footer.php"; ?>