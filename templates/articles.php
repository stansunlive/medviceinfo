<?php include "includes/header.php"; ?>
    <div id="bl-article">
    <?php foreach ($elements['articles'] as $article) { ?>
        <div class="bl-author st-t st-fs">
            <div class="bl-avatar">
                <img src="<?=$article->avatar?>" alt="Аватар"/>
            </div>
            <div class="bl-name"><?=$article->author?></div>
            <div class="bl-specialty st-tdark"><?=$article->specialization?></div>
            <div onclick="location.href='?action=view&id=<?=$article->id?>';" class="bl-material st-tblack bl-hover">
                <h1 class="bl-title"><?=$article->title?></h1>
                <p><?=$article->summary?></p>
                <img src="<?=$article->image?>" alt="article-img" height="400" class="article-img"/>
            </div>
            <a  class="st-btblue st-fs st-twhite st-bg st-tcenter" href="?action=view&id=<?=$article->id?>">Подробнее</a>
        </div>
    <?php } ?>
    </div>
<?php include "includes/footer.php"; ?>