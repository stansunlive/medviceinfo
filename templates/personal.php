<?php include "includes/header.php"; ?>
<?php $profile = $elements['profile']; ?>
<div title='Вернуться к разделу "Это интересно"' id="bl-hgoback" onclick="location.href='?action=none'" class="st-mi st-scroll-fix"></div>
<div id="bl-article">
    <div class="bl-author st-t st-fs">
        <div class="bl-avatar">
            <img src="<?=$profile->avatar ?>" alt="Аватар"/>
        </div>
        <div class="bl-name"><?= $profile->lastname . " " . $profile->firstname . " " . $profile->middlename?></div>
        <div class="bl-specialty st-tdark"><?= $profile->email ?></div>
        <div class="bl-material st-tblack">
            <h1 class="bl-title">Ваши статьи:</h1>
            <a href="?action=create" title="Написать новую статью" class="btn effect orange">Написать статью</a>
            <?php foreach (Article::getByAuthor($profile->username) as $article) { ?>
                    <h1 class="bl-title"><?= $article->title ?></h1>
                    <p><?= $article->summary ?></p>
                    <span>Статус статьи:</span>
                    <?php
                            $numb = (int)Article::getStatus($article->id);
                            if($numb == 1) {
                                echo "<span style='color:green;'>Одобрена</span>";
                            } else if($numb == 0) {
                                echo "<span style='color:#E0A200;'>Ожидает проверки</span>";
                            } else {
                                echo "<span style='color:red;'>Статья не одобрена</span>";
                            }
                    ?>
            <?php } ?>
        </div>
    </div>
</div>

<?php include "includes/footer.php"; ?>