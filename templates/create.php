<?php include "includes/header.php"; ?>
<script>

</script>
<div class="container_full">
    <div class="wrapper">
        <h1>Создание статьи</h1>
        <form enctype="multipart/form-data" class="crart" action="index.php?action=create" method="post">
            <input type="hidden" name="create" value="true"/> 
            <input class="st-input-light st-t st-fs lm-nk"
                       type="text"
                       name="title"
                       id="title"
                       placeholder="Введите заголовок статьи"
                       required autofocus maxlength="160"/>
            <textarea id="editor" name="content" cols="101" rows="40" width="100%"></textarea>
            <script type="text/javascript">
                var ckeditor = CKEDITOR.replace( 'editor' );
            </script>
            <input id="filef" class="fileField" type="file" name="artimage"
                   accept="image/jpeg,image/png"
                   onformchange="setValue();"
                   required/>
            <input class="btn effect orange right" required type="submit" name="send" value="Отправить"/>
        </form>
    </div>
</div>
<?php include "includes/footer.php"; ?>