<?php
class User
{
    public $username = null;
    public $email = null;
    public $phone = null;
    public $avatar = null;
    public $lastname = null;
    public $firstname = null;
    public $middlename = null;
    public $biography = null;
    public $experience = null;
    public $workplace = null;
    
    public function __construct($data = array()) {
        if(isset($data['username'])){
            $this->username = $data['username'];
        }
        if(isset($data['email'])){
            $this->email = $data['email'];
        }
        if(isset($data['phone'])){
            $this->phone = $data['phone'];
        }
        if(isset($data['avatar'])){
            $this->avatar = (AVATAR_PATH . $data['avatar']);
        }
        if(isset($data['lastname'])){
            $this->lastname = $data['lastname'];
        }
        if(isset($data['firstname'])){
            $this->firstname = $data['firstname'];
        }
        if(isset($data['middlename'])){
            $this->middlename = $data['middlename'];
        }
        if(isset($data['biography'])){
            $this->biography = $data['biography'];
        }
        if(isset($data['experience'])){
            $this->experience = $data['experience'];
        }
        if(isset($data['workplace'])){
            $this->workplace = $data['workplace'];
        }
    }
    
    public static function getUserId($username){
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT users_enc.id AS id FROM users_enc WHERE users_enc.username = :username";
        $query = $connection->prepare($sql);
        $query->bindValue(":username", $username, PDO::PARAM_STR);
        $query->execute();
        $id = $query->fetch();
        $id = $id['id'];
        $connection = null;
        return $id;
    }
    
    public static function getInfo($username){
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT users_enc.username AS username, users_enc.email AS email,
                users_enc.phone AS phone, users_enc.userphoto AS avatar, 
                doctor.lastname AS lastname, doctor.firstname AS firstname,
                doctor.middlename AS middlename, doctor.biography AS biography,
                doctor.experience AS experience, doctor.work_place AS workplace
                FROM users_enc LEFT JOIN doctor ON doctor.email = users_enc.email
                WHERE users_enc.username = :username";
        $query = $connection->prepare($sql);
        $query->bindValue(":username", $username, PDO::PARAM_STR);
        $query->execute();
        $connection = null;
        $info = $query->fetch();
        return new User($info);
    }
    
    public static function IsAuth($login, $password){
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT username AS login, password AS password FROM users_enc
                WHERE username = :lg";
        $query = $connection->prepare($sql);
        $query->bindValue(":lg", $login, PDO::PARAM_STR);
        $query->execute();
        $connection = null;
        $answer = $query->fetch();
        if($login == $answer['login'] && $password == $answer['password']){
            return true;
        } else {
            return $answer['login'];
        }
    }
}

