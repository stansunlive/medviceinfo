<?php
class Article
{
    public $id = null;
    public $user_id = null;
    public $author = null;
    public $avatar = null;
    public $date = null;
    public $title = null;
    public $summary = null;
    public $content = null;
    public $image = null;
    public $specialization = null;
    
    public function __construct($data = array()) {
        if (isset($data['id'])) { $this->id = (int) $data['id']; }
        if (isset($data['user_id'])) { $this->user_id = (int) $data['user_id']; }
        if (isset($data['authorname']) && isset($data['authorfamily'])) {
            $this->author = $data['authorfamily'] . " " . ($data['authorname'] . " " . $data["fatherName"]);  
        }
        if (isset($data['avatar'])) { $this->avatar = (AVATAR_PATH . $data['avatar']); }
        if (isset($data['publdate'])) { $this->date = (int) $data['publdate']; }
        if (isset($data['title'])) { $this->title = $data['title']; }
        if (isset($data['content'])) {
            $this->content = $data['content'];
            $this->summary = (mb_substr($data['content'], 0, 1484));
        }
        if (isset($data['image'])) { $this->image = (ARTICLES_IMAGES_PATH . $data['image']); }
        if (isset($data['specialization'])) { $this->specialization = $data['specialization']; }
    }
    
    public static function getById($id) {
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT article.article_id AS id, article.user_id AS user_id, article.article_title AS title,
                article.article_content AS content, article.article_date AS publdate,
                doctor.firstname AS authorname, doctor.lastname AS authorfamily,
                doctor.middlename AS fatherName,
                users_enc.userphoto AS avatar, article_images.article_img AS image,
                specialization.spec_name AS specialization
                FROM article LEFT JOIN users_enc ON users_enc.id = article.user_id
                LEFT JOIN doctor ON doctor.email = users_enc.email
                LEFT JOIN article_images ON article_images.article_id = article.article_id
                LEFT JOIN docsp ON docsp.id_doctor = users_enc.id
                LEFT JOIN specialization ON specialization.id_spec = docsp.id_spec
                WHERE article.article_id = :id and article.article_active = 1 and doctor.qualitycheck = 1";
        $query = $connection->prepare($sql);
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->execute();
        $connection = null;
        $result = $query->fetch();
        if($result){
            return new Article($result);
        }
    }
    
    public static function getByAuthor($author){
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT article.article_id AS id, article.article_title AS title,
                article.article_content AS content, article.article_date AS publdate
                FROM article WHERE user_id = (SELECT id FROM users_enc 
                WHERE username = :name)";
        $query = $connection->prepare($sql);
        $query->bindValue(":name", $author, PDO::PARAM_STR);
        $query->execute();
        
        $list = array();
        while($row = $query->fetch()){
            $result = new Article($row);
            $list[] = $result;
        }
        $connection = null;
        return $list;
    }
    
    public static function getList($page) {
        $max = (int)NUM_OF_ARTICLES;
        $start = (int)(($page * NUM_OF_ARTICLES) - NUM_OF_ARTICLES);
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT article.article_id AS id, article.user_id AS user_id, article.article_title AS title,
                article.article_content AS content, article.article_date AS publdate,
                doctor.firstname AS authorname, doctor.lastname AS authorfamily,
                doctor.middlename AS fatherName,
                users_enc.userphoto AS avatar, article_images.article_img AS image,
                specialization.spec_name AS specialization
                FROM article LEFT JOIN users_enc ON users_enc.id = article.user_id
                LEFT JOIN doctor ON doctor.email = users_enc.email
                LEFT JOIN article_images ON article_images.article_id = article.article_id
                LEFT JOIN docsp ON docsp.id_doctor = users_enc.id
                LEFT JOIN specialization ON specialization.id_spec = docsp.id_spec
                WHERE article.article_id > :startValue and article.article_active = 1 and doctor.qualitycheck = 1
                ORDER BY article.article_id desc LIMIT :max";
        $query = $connection->prepare($sql);
        $query->bindValue(":startValue", $start, PDO::PARAM_INT);
        $query->bindValue(":max", $max, PDO::PARAM_INT);
        $query->execute();
        
        $list = array();
        while($row = $query->fetch()){
            $result = new Article($row);
            $list[] = $result;
        }
        $connection = null;
        return $list;
    }
    
    public static function getListToModerate() {
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT article.article_id AS id, article.user_id AS user_id, article.article_title AS title,
                article.article_content AS content, article.article_date AS publdate,
                doctor.firstname AS authorname, doctor.lastname AS authorfamily,
                doctor.middlename AS fatherName,
                users_enc.userphoto AS avatar, article_images.article_img AS image,
                specialization.spec_name AS specialization
                FROM article LEFT JOIN users_enc ON users_enc.id = article.user_id
                LEFT JOIN doctor ON doctor.email = users_enc.email
                LEFT JOIN article_images ON article_images.article_id = article.article_id
                LEFT JOIN docsp ON docsp.id_doctor = users_enc.id
                LEFT JOIN specialization ON specialization.id_spec = docsp.id_spec
                WHERE doctor.qualitycheck = 1
                ORDER BY article.article_id desc";
        $query = $connection->prepare($sql);
        $query->execute();
        
        $list = array();
        while($row = $query->fetch()){
            $result = new Article($row);
            $list[] = $result;
        }
        $connection = null;
        return $list;
    }
    
    public function insert() {
        if(!is_null($this->id)){
            trigger_error("Ошибка метода Article::insert()... У нового объекта статьи не должно быть предопределенного ID.");
        }
        
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "INSERT INTO article (user_id, article_date, article_title, article_content, article_active)
                VALUES ( :user_id, NOW(), :article_title, :article_content, 0);
                INSERT INTO article_images (article_img)
                VALUES ( :image_ref )";
        $query = $connection->prepare($sql);
        $query->bindValue(":user_id", $this->user_id, PDO::PARAM_INT);
        $query->bindValue(":article_title", $this->title, PDO::PARAM_STR);
        $query->bindValue(":article_content", $this->content, PDO::PARAM_STR);
        $query->bindValue(":image_ref", $this->image, PDO::PARAM_STR);
        $query->execute();
        $this->id = $connection->lastInsertId();
        $connection = null;
    }
    
    public function update() {
        if(is_null($this->id)){
            trigger_error("Ошибка метода Article::update()... Невозможно редактировать статью у которой не определен ID.");
        }
        
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "UPDATE article SET article_title=:title, article_content=:content, article_active = 0
                WHERE id = :id;
                UPDATE article_images SET article_img = :image 
                WHERE id = :id";
        $query = $connection->prepare($sql);
        $query->bindValue(":title", $this->title, PDO::PARAM_STR);
        $query->bindValue(":content", $this->content, PDO::PARAM_STR);
        $query->bindValue(":image", $this->image, PDO::PARAM_STR);
        $query->bindValue(":id", $this->id, PDO::PARAM_INT);
        $query->execute();
        $connection = null;
    }
    
    public static function setStatus($id, $num) {
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "UPDATE article SET article_active=:num WHERE article_id = :id";
        $query = $connection->prepare($sql);
        $query->bindValue(":id", (int)$id, PDO::PARAM_INT);
        $query->bindValue(":num", (int)$num, PDO::PARAM_INT);
        $query->execute();
        $connection = null;
    }

    public static function getStatus($id) {
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "SELECT article_active FROM article WHERE article_id = :id";
        $query = $connection->prepare($sql);
        $query->bindValue(":id", (int)$id, PDO::PARAM_INT);
        $query->execute();
        $connection = null;
        $wtf = $query->fetch();
        return $wtf[0];
    }


    
    public function delete() {
        if(is_null($this->id)){
            trigger_error("Ошибка метода Article::delete()... Невозможно удалить статью у которой не определен ID.");
        }
        
        $connection = new PDO(dblink, dbusername, dbpassword);
        $sql = "DELETE FROM article WHERE article_id = :id LIMIT 1;
                DELETE FROM article WHERE article_id = 1 LIMIT 1;";
        $query = $connection->prepare($sql);
        $query->bindValue(":id", $this->id, PDO::PARAM_INT);
        $query->execute();
        $connection = null;
    }
}