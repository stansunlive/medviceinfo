<?php
ini_set("display_errors", true);
date_default_timezone_set("Europe/Moscow");
define("dblink", "mysql:host=localhost;dbname=medvice_db;charset=utf8");
define("dbusername", "root");
define("dbpassword", "");
define("CLASS_PATH", "classes/");
define("TEMPLATE_PATH", "templates/");
define("AVATAR_PATH", "images/avatars/");
define("ARTICLES_IMAGES_PATH", "images/articles/");
define("NUM_OF_ARTICLES", "5");
define("MODERATOR_USERNAME", "stansunlive");
define("MODERATOR_PASSWORD", "1e16qw3hn9");

require (CLASS_PATH . "article.php");
require (CLASS_PATH . "user.php");

function handleException ( $exception ) {
    echo "Произошла ошибка... Но какая...</br>";
    echo $exception->getMessage();
}

set_exception_handler('handleException');
?>