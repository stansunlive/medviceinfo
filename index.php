<?php
require 'config.php';
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset($_SESSION['username']) ? $_SESSION['username'] : "";
$status = null;

switch($action) {
    case 'view':
        viewArticle();
        break;
    case 'personal':
        personal();
        break;
    case 'create':
        create();
        break;
    case 'logout':
        logout();
        break;
    case 'delete':
        delete();
        break;
    case 'update':
        delete();
        break;
    default :
        mainpage();
        break;
}

function viewArticle() {
    if(!isset($_GET["id"]) && !$_GET["id"]) {
        homepage();
        return;
    }
    
    $elements = array();
    $elements['article'] = Article::getById((int)$_GET["id"]);
    $elements['title'] = $elements['article']->title;
    require (TEMPLATE_PATH . "article.php");
}

function create(){
    if($_SESSION['auth'] === false){
        login();
        exit;
    }
    
    $elements = array();
    $elements['title'] = "Создание статьи";
    if(isset($_POST['create'])){
        if(isset($_POST['title']) && isset($_POST['content'])){
            $a = new Article();
            $a->user_id = User::getUserId($_SESSION['username']);
            $a->title = $_POST['title'];
            $a->content = $_POST['content'];
            if($_FILES["artimage"]["size"] > 1024*1*1024)
            {
                echo ("Размер файла превышает три мегабайта");
                $status = "Вы загрузили слишком большой файл...";
                exit;
            }
            if(is_uploaded_file($_FILES["artimage"]["tmp_name"]))
            {
                move_uploaded_file($_FILES["artimage"]["tmp_name"], (ARTICLES_IMAGES_PATH . $_FILES["artimage"]["name"]));
            } else {
                $status = "Ошибка загрузки файла...";
                exit;
            }
            $a->image = $_FILES['artimage']['name'];
            $a->insert();
            $status = "Статья отправленна на проверку модераторам.";
            header("Location:index.php?action=personal");
        }
    }
    require (TEMPLATE_PATH . "create.php");
}

function login() {
    $elements = array();
    if($_SESSION['auth']){
        personal();
        exit;
    } else {
        $elements['title'] = "Вход для пользователей...";
        if(isset($_POST['login'])){
            if(User::IsAuth($_POST['login'], $_POST['password'])){
                $_SESSION['username'] = $_POST['login'];
                $_SESSION['auth'] = true;
                header("Location:index.php?action=personal");
                
            } else {
                $elements['error'] = "Неправильный логин или пароль";
                require (TEMPLATE_PATH . 'loginform.php');
            }
        } else {
            require (TEMPLATE_PATH . 'loginform.php');
        }
    }
}

function logout() {
    unset($_SESSION['username']);
    $_SESSION['auth'] = false;
    header("Location:index.php");
}

function personal() {
    if($_SESSION['auth'] === false){
        login();
        exit;
    }
    
    $elements['title'] = ("Личный кабинет пользователя " . $_SESSION['username']);
    $elements['profile'] = User::getInfo($_SESSION['username']);
    require (TEMPLATE_PATH . 'personal.php');
}

function delete(){
    $a = Article::getById($_GET['id']);
    if(User::getUserId($_SESSION['username']) != $a->user_id || !$_SESSION['auth']) {
        echo "Нет прав на удаление файла";
    } else {
        $a->delete();
        mainpage();
    }
}

function mainpage() {
    $elements = array();
    $elements['page'] = (isset($_GET["page"])) ? (int)$_GET["page"] : 1;
    $elements['articles'] = Article::getList($elements['page']);
    $elements['title'] = "Раздел: Это интересно.";
    require (TEMPLATE_PATH . "articles.php");
}
?>  