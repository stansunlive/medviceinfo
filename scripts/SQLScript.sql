DROP TABLE IF EXISTS users_enc;
CREATE TABLE users_enc
(
id              bigint(20) unsigned NOT NULL auto_increment,
username        varchar(20) NOT NULL,
password        char(60) NOT NULL,
email           varchar(60) NOT NULL,
phone           varchar(20) NOT NULL,
secQ            varchar(32) NOT NULL,
secA            int(1) NOT NULL,
userphoto       char(29),
userurl         text,

PRIMARY KEY     (id)
);

DROP TABLE IF EXISTS doctor;
CREATE TABLE doctor
(
email           varchar(60) NOT NULL,
lastname        text NOT NULL,
firstname       text NOT NULL,
middlename      text NOT NULL,
id_city         int(11),
category        int(1),
qualitycheck    int(1),
biography       text,
experience      int(2),
work_place      varchar(255),
courses         varchar(255)
);

DROP TABLE IF EXISTS specialization;
CREATE TABLE specialization
(
id_spec         int(3) NOT NULL,
spec_name       varchar(60),
description     varchar(150)
);

DROP TABLE IF EXISTS article;
CREATE TABLE article
(
article_id      bigint(20) unsigned NOT NULL auto_increment,
user_id         int(20) NOT NULL,
article_date    date NOT NULL,
article_title   varchar(255),
article_content mediumtext NOT NULL,
article_active  tinyint(1) NOT NULL,

PRIMARY KEY     (article_id)
);

DROP TABLE IF EXISTS article_images;
CREATE TABLE article_images
(
article_id      int(20) unsigned NOT NULL auto_increment,
article_img     varchar(60),

PRIMARY KEY (article_id)
);

DROP TABLE IF EXISTS docsp;
CREATE TABLE docsp
(
id_spec         int(3),
id_doctor       bigint(20),
del             int(1)
);